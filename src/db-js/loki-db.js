import loki from "lokijs";
import LokiCordovaFSAdapter from "loki-cordova-fs-adapter";
// import LokiIndexedAdapter from "lokijs/src/loki-indexed-adapter";
var __db__;
export default {
  Initialize: function(filename, callback) {
    // var pa = new loki.LokiPartitioningAdapter(new LokiIndexedAdapter(), { paging: true });
    var pa = new LokiCordovaFSAdapter({ prefix: "v_wall" });
    __db__ = new loki(filename.replace(/[^a-zA-Z0-9_-]/g, "_"), {
      autoload: true,
      autosave: true,
      autosaveInterval: 250,
      env: "BROWSER",
      //adapter: pa,
      unique: ["id"]
    });
    __applog("lokijs...initialize..." + JSON.stringify(__db__));
    callback(true);
  },
  CollectionHandlerData: function(table, callback) {
    let existingCollection = __db__.getCollection(table);
    if (existingCollection == null) {
      __applog("Creating collection Data: " + table);
      existingCollection = __db__.addCollection(table);
    }
    callback(existingCollection.data);
  },
  CollectionHandler: function(table, callback) {
    let existingCollection = __db__.getCollection(table);
    if (existingCollection == null) {
      __applog("Creating collection: " + table);
      existingCollection = __db__.addCollection(table);
    }
    callback(existingCollection);
  },
  InsertData: function(table, data, obj) {
    try {
      if (obj != null) {
        table.insert(data);
      } else {
        this.CollectionHandler(table, holder => {
          holder.insert(data);
        });
      }
    } catch (e) {
      __applog("Insert Data Error: " + e);
    }
  },
  AddToCollection: function(table, data, callback) {
    if (data != null) {
      this.CollectionHandler(table, holder => {
        this.ScalarQuery(
          table,
          {
            id: data.id
          },
          response => {
            if (response == null) {
              __applog(
                "Pushing new data...to " +
                  table +
                  " with data of " +
                  JSON.stringify(data)
              );
              if (data instanceof Array) {
                __applog("Type is array...Iterate...");
                __vueinstance__.$_lodash.forEach(data, response => {
                  let ispresent = holder.findOne({
                    id: response.id
                  });
                  if (ispresent == null) {
                    this.InsertData(holder, response, 1);
                  }
                });
              } else {
                __applog("Data ID: " + data.id);
                this.ScalarQuery(
                  holder,
                  {
                    id: data.id
                  },
                  response => {
                    if (response == null) {
                      this.InsertData(holder, data, 1);
                    }
                  }
                );
              }
            }
            callback(true);
          }
        );
      });
    } else {
      callback(false);
    }
  },
  ScalarQuery: function(table, query, callback) {
    this.CollectionHandler(table, holder => {
      callback(holder.findOne(query));
    });
  },
  GetFirst: function(table, callback) {
    this.CollectionHandler(table, holder => {
      callback(holder.get(1));
    });
  },
  ListQuery: function(table, query, callback) {
    this.CollectionHandler(table, holder => {
      callback(holder.find(query).simplesort("id"));
    });
  },
  UpdateEntry: function(table, query, data, callback) {
    this.CollectionHandler(table, holder => {
      this.ScalarQuery(table, query, _data => {
        if (_data != null) {
          __applog("Find Entry: " + query);
          holder.update(data);
          callback(holder);
        } else {
          __applog("Insert Entry: " + query);
          this.AddToCollection(table, data, response => {
            callback(response);
          });
        }
      });
    });
  }
};
