export default {
  BlobConvert(b64Data, contentType, sliceSize) {
    contentType = contentType || "";
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  },
  CreateLocalDirectory(folderpath, subdir, callback) {
    window.resolveLocalFileSystemURL(folderpath, dir => {
      if (subdir != null) {
        dir.getDirectory(subdir, { create: true }, _subdir => {
          __applog("Directory Created: " + JSON.stringify(_subdir));
          callback(_subdir);
        });
        return;
      }
      callback(dir);
    });
  },
  FileToBase64(path, callback) {
    window.resolveLocalFileSystemURL(
      path,
      fileEntry => {
        fileEntry.file(file => {
          var reader = new FileReader();
          reader.onloadend = function(e) {
            var content = this.result;
            callback(content);
          };
          reader.readAsDataURL(file);
        });
      },
      fail => {
        __applog(fail + " ==> " + path);
      }
    );
  },
  SaveBase64AsImageFile(folderpath, filename, content, contentType, callback) {
    var DataBlob = this.BlobConvert(content, contentType);
    this.CreateLocalDirectory(folderpath, null, dir => {
      console.log("Access to the directory granted succesfully");
      dir.getFile(filename, { create: true }, file => {
        console.log("File created succesfully.");
        file.createWriter(
          fileWriter => {
            console.log("Writing content to file");
            fileWriter.onwriteend = () => {
              console.log("Successful file write...");
              callback(true);
            };
            fileWriter.write(DataBlob);
          },
          () => {
            __applog("Unable to save file in path " + folderpath);
            callback(false);
          }
        );
      });
    });
  }
};
