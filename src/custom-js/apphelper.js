export default {
  Initialize() {
    //put any initialization here
  },
  LogoutWithMessage(hasMessage) {
    __vueinstance__.$root._isloggingout = true; //show logout loader
    window.CacheClear(
      function (status) {
        //cookie clear
        __applog("Clearing Cookies");
        var cookies = document.cookie.split("; ");
        for (var c = 0; c < cookies.length; c++) {
          var d = window.location.hostname.split(".");
          while (d.length > 0) {
            var cookieBase =
              encodeURIComponent(cookies[c].split(";")[0].split("=")[0]) +
              "=; expires=Thu, 01-Jan-1970 00:00:01 GMT; domain=" +
              d.join(".") +
              " ;path=";
            var p = location.pathname.split("/");
            document.cookie = cookieBase + "/";
            while (p.length > 0) {
              document.cookie = cookieBase + p.join("/");
              p.pop();
            }
            d.shift();
          }
        }
        if (hasMessage) alert("Please restart the application...");

        let deviceID = localStorage.getItem(_device_id_storage());
        let username = localStorage.getItem(_user_id_storage());

        __applog("DeviceID: " + deviceID);
        __applog("Username: " + username);

        let pTag = localStorage.getItem(_phone_tag());
        if (!pTag) {
          //non rooted
          window.open(
            LOGOUT_URI + encodeURIComponent(REDIRECT_URI),
            "_blank",
            "location=no,toolbar=no,clearcache=yes,clearsessioncache=yes,hidden=yes"
          );
          __authContext__.tokenCache.clear();
        } else {
          window.open(
            "about:blank",
            "_blank",
            "location=no,toolbar=no,clearcache=yes,clearsessioncache=yes,hidden=yes"
          );
        }

        __vueinstance__.$_httphelper
          .IHttpGet("Logout", {
            name: username,
            deviceID: deviceID,
            client: "axios"
          })
          .then(logoutResponse => {
            __applog("Logout Executed..." + logoutResponse);
            //clear all local storage entry
            cordova.plugins.backgroundMode.un("deactivate", () => {
              __applog("Deactivation successful...");
            });
            cordova.plugins.backgroundMode.disable(); //disable
            if (__authContext__ != null) __authContext__.logOut();

            localStorage.clear();

            //exit the application
            if (typeof cordova !== "undefined") {
              if (navigator.app) {
                navigator.app.exitApp();
              } else if (navigator.device) {
                navigator.device.exitApp();
              }
            } else {
              window.close();
              $timeout(function () {
                self.showCloseMessage = true;
              });
            }
          });
      },
      function (status) {
        __applog("Error: " + status);
      }
    );
  }
};
