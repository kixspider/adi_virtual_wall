var blockOwnerJson = _block_owner_json();
var imageJson = _image_file_json();
export default {
  ImageToBas64Format(qrcode, callback) {
    __applog("Checking from local record...");
    __vueinstance__.$_lokijs.ScalarQuery(
      imageJson,
      {
        id: qrcode
      },
      response => {
        if (response != null) {
          __applog("Image Data: " + JSON.stringify(response));
          callback(response);
        } else {
          this.GetQRImage(qrcode, data => {
            callback(data);
          });
        }
      }
    );
  },
  GetQRImage(qrcode, callback) {
    __vueinstance__.$_httphelper
      .IHttpPost("GetImage", {
        QRCode: qrcode
      })
      .then(b64JsonContent => {
        let ntAccount = b64JsonContent["ntAccount"];
        let blockOwnerID = b64JsonContent["BlockOwnerID"];
        __applog("Detected Nt Account: " + ntAccount);

        //employee
        var blockOwner = {
          id: blockOwnerID,
          BlockOwnerID: blockOwnerID,
          NtAccount: ntAccount,
          FirstName: b64JsonContent["FirstName"],
          LastName: b64JsonContent["LastName"]
        };

        let _image = b64JsonContent["Image"];

        //block
        var block = {
          id: qrcode,
          BlockID: b64JsonContent["BlockID"],
          QRCode: qrcode,
          Image: _image,
          BlockOwnerID: b64JsonContent["BlockOwnerID"]
        };

        if (_image !== "" && ntAccount !== "") {
          let key = ntAccount + "-" + blockOwnerID;
          __vueinstance__.$_lokijs.UpdateEntry(
            blockOwnerJson,
            {
              id: key
            },
            blockOwner,
            response => {
              __applog("GetQRImage: " + JSON.stringify(response));
              if (response.data != null) {
                __applog("Response: with data / Blockowner");
                callback(block);
              } else {
                __applog("Response: no data / Blockowner");
                this.ImageOperation(qrcode, block, data => {
                  callback(data);
                });
              }
            }
          );
        } else {
          callback("IMAGE_ERROR");
        }
      });
  },
  ImageOperation(qrcode, block, callback) {
    let _path = cordova.file.dataDirectory;
    __vueinstance__.$_imageconverter.CreateLocalDirectory(
      _path,
      "qr_image_path",
      dir => {
        if (dir != null) {
          let nPath = _path + dir.fullPath;
          let fname = qrcode + ".png";
          __vueinstance__.$_imageconverter.SaveBase64AsImageFile(
            nPath,
            fname,
            block.Image,
            "image/png",
            _response => {
              if (_response) {
                block.Image = nPath + fname; //update image path
                __vueinstance__.$_lokijs.UpdateEntry(
                  imageJson,
                  { id: qrcode },
                  block,
                  response => {
                    if (response.data != null) {
                      callback(response.data[qrcode]);
                    } else {
                      callback(block);
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  }
};
