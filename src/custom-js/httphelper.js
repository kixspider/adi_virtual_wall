import axios from "axios";
const CancelToken = axios.CancelToken;
const source = CancelToken.source();
export default {
  IHttpPost(url, data) {
    data = JSON.stringify(data);
    __applog("Post Data: " + data);
    return axios
      .post(url, data, this.AxiosConfig())
      .then(response => {
        try {
          return JSON.parse(response.data);
        } catch (e) {
          return response.data;
        }
      })
      .catch(error => {
        __applog("Axios|Error|POST: " + error);
        this.ExitWin();
        return "AXIOS_ERROR";
      });
  },
  IHttpGet(url, params) {
    return axios
      .get(
        url,
        {
          params: params
        },
        this.AxiosConfig()
      )
      .then(response => {
        try {
          return JSON.parse(response.data);
        } catch (e) {
          return response.data;
        }
      })
      .catch(error => {
        __applog("Axios|Error|GET: " + error);
        this.ExitWin();
        return "AXIOS_ERROR";
      });
  },
  Initialize() {
    axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
    axios.defaults.baseURL = BASEURI + "api.svc/";
    axios.interceptors.request.use(
      config => {
        //__applog("Interceptor Request: " + JSON.stringify(config));
        return config;
      },
      error => {
        return Promise.reject(error);
      }
    );
    // Add a response interceptor
    axios.interceptors.response.use(
      response => {
        //__applog("Interceptor Response: " + JSON.stringify(response));
        __vueinstance__.$root.progress_total = 0;
        __vueinstance__.$root.progress_loaded = 0;
        return response;
      },
      error => {
        return Promise.reject(error);
      }
    );
  },
  AxiosConfig() {
    var config = {
      onUploadProgress: function (progressEvent) {
        __vueinstance__.$root.progress_total = progressEvent.total;
        __vueinstance__.$root.progress_loaded = progressEvent.loaded;
      },
      onDownloadProgress: function (progressEvent) {
        __vueinstance__.$root.progress_total = progressEvent.total;
        __vueinstance__.$root.progress_loaded = progressEvent.loaded;
      },
      validateStatus: function (status) {
        __applog("Status: " + status);
        return status >= 200 && status < 300;
      },
      // responseType: "json",
      // responseEncoding: "utf8",
      timeout: 1000 * 60 * 10,//10 minutes timeout for slow internet connection
      cancelToken: source.token,
      headers: {
        "Content-Type": "application/json;charset=UTF-8",
        "Access-Control-Allow-Origin": "*"
      },
      crossdomain: true
    };

    return config;
  },
  CancelRequest() {
    __applog("Request cancel...");
    source.cancel("Operation canceled by the user!");
  },
  ExitWin(msg) {
    //exit  //exit the application
    this.CancelRequest();
    alert(
      msg == null
        ? "Network communication error. Please make sure you have an stable network connection."
        : msg
    );
    if (typeof cordova !== "undefined") {
      if (navigator.app) {
        navigator.app.exitApp();
      } else if (navigator.device) {
        navigator.device.exitApp();
      }
    } else {
      window.close();
      $timeout(() => {
        self.showCloseMessage = true;
      });
    }
  }
};
