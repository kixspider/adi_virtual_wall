//advirtualwall@gmail.com
export default {
  Initialize() {
    document.addEventListener(
      "deviceready",
      this.onDeviceReady.bind(this),
      false
    );
    window.addEventListener("load", this.onLoad.bind(this), false);
    window.addEventListener(
      "filePluginIsReady",
      this.onFilePlugin.bind(this),
      false
    );
  },
  onDeviceReady() {
    //device ready event
    __applog("Device Ready Event...");
    //local storage
    localStorage.setItem(_device_id_storage(), device.serial);
    IRoot.isRooted(
      success => {
        localStorage.setItem(_phone_tag(), success);
        if (success) {
          this.PlainJSADAL(response => {
            if (response === "OK") {
              this.TokenAction();
            } else if (response === "ERROR") {
              __vueinstance__.$_httphelper.ExitWin();
            }
          });
        } else {
          this.MsADAL(authResponse => {
            __applog("MS ADAL Response: " + authResponse);
            if (authResponse === "OK") {
              this.PhoneStatusCheck(false, response => {
                if (response === "OK") {
                  this.TokenAction();
                } else if (response === "ERROR") {
                  __vueinstance__.$_httphelper.ExitWin();
                }
              });
            }
          });
        }
      },
      fail => {
        __applog("Root check..." + fail);
      }
    );
  },
  onLoad() {
    //onload event
    __applog("Onload Event...");
  },

  TokenAction() {
    __vueinstance__.$_lokijs.Initialize(
      localStorage.getItem(_user_email()),
      response => {
        if (response) {
          __vueinstance__.$_signalr.Initialize(BASEURI, (status, token) => {
            if (status === "CONNECTED") {
              localStorage.setItem(_con_id(), encodeURIComponent(token));
              this.LoginTask();
            } else if (status === "NO_CONNECTED") {
              __vueinstance__.$_httphelper.ExitWin();
            }
          });
        }
      }
    );
  },
  PhoneStatusCheck(bPlain, callback) {
    __applog("Host check: " + bPlain);
    var _keystring = __vueinstance__.$root.keystring;
    navigator.splashscreen.show(); //show the splash screen
    var ref = window.open(
      BASEURI + "api.svc/test?e=" + _keystring,
      "_blank",
      "location=no,toolbar=no,zoom=no,hardwareback=yes"
    );
    ref.addEventListener("loaderror", event => {
      __applog("Load Error: " + event.url);
      __vueinstance__.$_httphelper.ExitWin();
    });
    ref.addEventListener("loadstart", event => {
      __applog("Load Start: " + event.url);
      navigator.notification.activityStart("", "Please wait....");
    });
    ref.addEventListener("loadstop", event => {
      __applog("Load Stop: " + event.url);
      if (bPlain) {
        if (event.url != "" && event.url.indexOf("&username=") > -1) {
          var qstring = query_string("username", event.url);
          if (qstring != null) {
            localStorage.setItem(_user_email(), qstring);
          }
        }
      }
      if (event.url != "" && event.url.indexOf(REDIRECT_URI) == 0) {
        //#region Testing
        ref.executeScript(
          {
            code: "document.body.innerHTML"
          },
          values => {
            let response = "OK";
            if (_keystring != "" && _keystring.length > 10)
              _keystring = _keystring.substr(0, 10); //trim the size to 10 character only
            if (
              values != null &&
              values.length > 0 &&
              values[0].indexOf(_keystring) > -1
            ) {
              __applog(values[0]);
              localStorage.setItem(
                _user_email(),
                localStorage.getItem(_user_email()).replace("@", "_")
              );
            } else if (values[0].indexOf("Webpage not available" > -1)) {
              response = "ERROR";
            }
            ref.close();
            callback(response);
          }
        );
        //#endregion
      }
      navigator.notification.activityStop();
    });
  },

  PlainJSADAL(callback) {
    if (typeof localStorage != undefined) {
      if (
        localStorage.getItem("adal.nonce.idtoken") ||
        localStorage.getItem("adal.state.login")
      ) {
        localStorage.removeItem("adal.nonce.idtoken");
        localStorage.removeItem("adal.state.login");
      }
    }
    __applog("Initialize PlainJS ADAL");
    Logging.piiLoggingEnabled = true;
    window.config = {
      clientId: CLIENT_ID,
      cacheLocation: "localStorage",
      popUp: true,
      expireOffsetSeconds: 3600,
      loadFrameTimeout: 60,
      displayCall: url => {
        __applog("connectivity check: " + url);
        this.PhoneStatusCheck(true, response => {
          callback(response);
        });
      }
    };
    __authContext__ = null;
    __authContext__ = new AuthenticationContext(config);
    __authContext__.login();
  },

  MsADAL(callback) {
    __applog("Ms ADAL login called...");
    Microsoft.ADAL.AuthenticationSettings.setLogger(function logger(logItem) {
      __applog("Logger: " + JSON.stringify(logItem));
    });
    __authContext__ = new Microsoft.ADAL.AuthenticationContext(AUTHORITY_URL);
    __authContext__.tokenCache.readItems().then(items => {
      __applog("Token Length: " + items.length);
      if (items.length > 0) {
        let authority = items[0].authority;
        __authContext__ = new Microsoft.ADAL.AuthenticationContext(authority);
      }
      __authContext__.acquireTokenSilentAsync(RESOURCE, CLIENT_ID).then(
        authResponse => {
          // let email = authResponse.userInfo.uniqueId.replace("@", "_");
          // localStorage.setItem(_user_email(), email);
          __applog("Silent: Expires on: " + authResponse.expiresOn);
          callback("OK");
        },
        err => {
          __applog("Silent Err: " + err);
          __authContext__
            .acquireTokenAsync(RESOURCE, CLIENT_ID, REDIRECT_URI)
            .then(
              authResponse => {
                __applog("Create: Expires on: " + authResponse.expiresOn);
                let email = authResponse.userInfo.uniqueId.replace("@", "_");
                localStorage.setItem(_user_email(), email);
                callback("OK");
              },
              err => {
                __applog("Error Create: " + err);
                callback("ERROR");
              }
            );
        }
      );
    });
  },
  LoginTask() {
    navigator.splashscreen.hide(); //hide the splahs screen
    __vueinstance__.$root.is_qr_checking = true; //show the loader
    __vueinstance__.$_httphelper
      .IHttpGet("GetInfo", {
        email: localStorage.getItem(_user_email())
      })
      .then(getUser => {
        var userInfo = getUser.data;
        if (typeof userInfo !== "undefined") {
          localStorage.setItem(_user_id_storage(), userInfo.NTAccount);
          let conid = localStorage.getItem(_con_id());
          __vueinstance__.$_lokijs.AddToCollection(
            _user_info_json(),
            {
              id: userInfo.NTAccount,
              NTAccount: userInfo.NTAccount,
              Fname: userInfo.FirstName,
              Lname: userInfo.LastName,
              Email: userInfo.EmailAddress,
              ID: userInfo.ID,
              ActiveConnectionID: conid
            },
            response => {
              if (response) {
                let password = "";
                let loginPost = {
                  user: userInfo.NTAccount,
                  password: password,
                  connectionID: conid,
                  deviceID: device.serial
                };
                __vueinstance__.$_httphelper
                  .IHttpPost("LoginInfo", loginPost)
                  .then(loginResponse => {
                    if (loginResponse != null) {
                      let _data = loginResponse;
                      if (_data.IsAuthenticated) {
                        __applog("User is authenticated...");
                        let _dateLastUpdate =
                          _data.ResponseContent.DateLastUpdated;
                        this.GetContentBasedOnDateLastUpdated(
                          _dateLastUpdate,
                          getdata => {
                            __applog("GetData: " + getdata);
                            __vueinstance__.$root.is_qr_checking = false; //hide the loader
                            if (getdata === "OK" || getdata === "NO_CHANGE") {
                              __vueinstance__.$router.push(
                                "/mymessage/" + _dateLastUpdate
                              );
                            }
                          }
                        );
                      } else {
                        __vueinstance__.$_httphelper.ExitWin(
                          "User authentication failure. Please reopen the application and try again!"
                        );
                      }
                    }
                  });
              }
            }
          );
        } else {
          __vueinstance__.$_httphelper.ExitWin();
        }
      });
  },
  GetContentBasedOnDateLastUpdated(datelastupdated, callback) {
    __applog("Retrieved from server: " + datelastupdated);
    if (datelastupdated != null) {
      let isOutdated = false;
      let dateLastUpdated_local = localStorage.getItem("dateLastUpdated");
      if (
        dateLastUpdated_local == null ||
        __vueinstance__.$root.momentDateCompare(
          dateLastUpdated_local,
          datelastupdated
        )
      ) {
        isOutdated = true;
        dateLastUpdated_local = datelastupdated;
      }
      __applog("Is Outdated: " + isOutdated);
      __applog("Using Method from index_signalr");
      if (isOutdated) {
        let contentPost = {
          ntAccount: localStorage.getItem(_user_id_storage()),
          deviceID: device.serial,
          dateLastUpdated: dateLastUpdated_local
        };
        __vueinstance__.$_httphelper
          .IHttpPost("PostContent", contentPost)
          .then(result => {
            if (result != null) {
              __applog("Store to image/signalrjs: " + _inbox_json());
              __vueinstance__.$_lodash.forEach(result.qrcodes, arr => {
                let qrcode = arr.id;
                __applog("Captured QR: " + JSON.stringify(arr));
                var block = {
                  id: qrcode,
                  BlockID: arr.BlockID,
                  QRCode: qrcode,
                  Image: arr.Image,
                  BlockOwnerID: arr.BlockOwnerID
                };
                __vueinstance__.$_imagehelper.ImageOperation(
                  arr.id,
                  block,
                  _response => {
                    if (_response != null) {
                      __vueinstance__.$_lokijs.AddToCollection(
                        _inbox_json(),
                        result.inbox,
                        response => {
                          if (response) {
                            localStorage.setItem("haslastupdated", true);
                            //store data
                            datelastupdated = __vueinstance__.$root.momentDate(
                              datelastupdated
                            );
                            __applog(
                              "Store date last updated to storage: " +
                              datelastupdated
                            );
                            localStorage.setItem(
                              "dateLastUpdated",
                              datelastupdated
                            );
                          }
                          callback("OK");
                        }
                      );
                    }
                  }
                );
              });
            } else {
              callback("NO_CHANGE");
            }
          });
      } else {
        callback("NO_CHANGE");
      }
      __vueinstance__.$_signalr.EnableBackgroundMode(true, result => {
        if (result === "OK") {
          setTimeout(() => {
            __vueinstance__.$_signalr.SetPushNotification(
              "Enterprise Alignment Virtual Wall",
              "Monitoring...",
              true
            );
          }, 5000);

          cordova.plugins.backgroundMode.on("enable", () => {
            __applog("Notifier enable...");
            var toggle = setInterval(() => {
              __vueinstance__.$_httphelper
                .IHttpGet("test", {
                  e: "heartbeat...ok"
                })
                .then(result => {
                  if (result.E == "heartbeat...ok") {
                    __applog("Ping Result: " + JSON.stringify(result));
                  } else {
                    clearInterval(toggle);
                    __vueinstance__.$_httphelper.ExitWin();
                  }
                });
            }, 1000 * 90);
          });
          cordova.plugins.backgroundMode.on("activate", () => {
            __applog(
              "Background: activated...Ping connectivity once connected..."
            );
            cordova.plugins.notification.badge.increase();
          });

          cordova.plugins.backgroundMode.on("deactivate", () => {
            __applog("Background: deactivated...");
            cordova.plugins.notification.badge.clear();
            __vueinstance__.$_signalr.SetPushNotification(
              "Enterprise Wall Alignment",
              "Monitoring....",
              true
            );
          });
        }
      });
    }
  },
  onFilePlugin() {
    __applog("File plugin is ready @ chrome");
  }
};
