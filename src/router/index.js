import Vue from "vue";
import Router from "vue-router";
import Home from "@/components/Home";
import NewMessage from "@/components/NewMessage";
import ImageView from "@/components/ImageView";
import Conversation from "@/components/Conversation";
import MyMessage from "@/components/MyMessage";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/newmessage/:qrcode",
      name: "NewMessage",
      component: NewMessage,
      props: true
    },
    {
      path: "/imageview/:qrcode",
      name: "ImageView",
      component: ImageView,
      props: true
    },
    {
      path: "/conversation/:qid/:subject/:isread/:qrcode/:tag?",
      name: "Conversation",
      component: Conversation,
      props: true
    },
    {
      path: "/mymessage/:id?",
      name: "MyMessage",
      component: MyMessage,
      props: true
    }
  ]
});
