/**
 * Initial release
 * Emerson Rodulfo | Engineering Systems
 *
 * Abraham Lincoln once said: "If I had six hours to chop down a tree, I'd spend the first four hours sharpening the axe".
 */
import Vue from "vue";

import "expose-loader?$!expose-loader?jQuery!jquery";

import "bootstrap/dist/js/bootstrap.min.js";
import "bootstrap/dist/css/bootstrap.min.css";

import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all.js";

//libs
import _randomgen from "randomstring";
import _lodash from "lodash";
import _moment from "moment/moment";
import _bootbox from "bootbox/bootbox";

// import "image-compressor";

import "./custom-css/loader.css";
import "./custom-css/animate.css";

import App from "./App";
import router from "./router";

/* Place custom component here */
import loadercomponent from "./components/LoaderPage.vue";
import headercomponent from "./components/Header.vue";
import footercomponent from "./components/Footer.vue";

//import event
import _lokijs from "./db-js/loki-db";
import _indexsignalr from "./custom-js/index_signalr.js";
import _httphelper from "./custom-js/httphelper.js";
import _signalr from "./fcm-js/signalRpush.js";
import _imageconverter from "./custom-js/imageconverter.js";
import _imagehelper from "./custom-js/imagehelper.js";
import _apphelper from "./custom-js/apphelper.js";

Vue.component("header-component", headercomponent);
Vue.component("footer-component", footercomponent);
Vue.component("loader-component", loadercomponent);
/* End of Cusotom Components */

Vue.prototype.$_bootbox = _bootbox;
Vue.prototype.$_moment = _moment;
Vue.prototype.$_lodash = _lodash;
Vue.prototype.$_lokijs = _lokijs;
Vue.prototype.$_httphelper = _httphelper;
Vue.prototype.$_signalr = _signalr;
Vue.prototype.$_imagehelper = _imagehelper;
Vue.prototype.$_imageconverter = _imageconverter;
Vue.prototype.$_apphelper = _apphelper;
Vue.prototype.$_indexsignalr = _indexsignalr;

/* do not show component */
Vue.config.productionTip = false;

/* eslint-disable no-new */
__vueinstance__ = new Vue({
  el: "#app",
  router,
  components: { App },
  watch: {
    is_qr_checking() {
      __applog("is_qr_checking: " + this.is_qr_checking);
    },
    progress_total() {
      __applog("Progress Total: " + this.progress_total);
    },
    progress_loaded() {
      __applog("Progress Loaded: " + this.progress_loaded);
    },
    isloggingout() {
      __applog("Is Logging Out: " + this.isloggingout);
    },
    keystring() {
      __applog("Generated Key: " + this.keystring);
    }
  },
  data: {
    /* This can be use as global variable to be used inside the application*/
    is_qr_checking: false,
    progress_total: 0,
    progress_loaded: 0,
    isloggingout: false,

    //string for use in url arrays
    keystring: ""
  },
  template: "<App/>",
  methods: {
    CompressImage(img, callback) {
      // if (img !== "") {
      //   try {
      //     _imageconverter.FileToBase64(img, data => {
      //       if (data != null) {
      //         let imageCompressor = new ImageCompressor();
      //         let compressor_settings = {
      //           toWidth: window.screen.width,
      //           toHeight: 200,
      //           mimeType: "image/png",
      //           mode: "strict",
      //           quality: 0.6,
      //           grayScale: false,
      //           sepia: false,
      //           threshold: false,
      //           vReverse: false,
      //           hReverse: false,
      //           speed: "high"
      //         };
      //         imageCompressor.run(data, compressor_settings, compressResult => {
      //           __applog("Compressed SRC: " + compressResult);
      //           callback(compressResult);
      //         });
      //       } else {
      //         __applog("Compress Data: NULL");
      //         callback(img); //no change
      //       }
      //     });
      //   } catch (e) {
      //     __applog("Compress Error: " + e);
      //     callback(img);
      //   }
      // } else {
      //   //no processing.
      //   callback(img);
      // }
      callback(img); //no change
    },
    FixDateEntry(_date) {
      if (_date != null) {
        try {
          _date = _date.replace(" ", "T");
          __applog("Fix Date: " + _date);
        } catch (e) {
          __applog("Ref Date: " + e);
        }
      }
      return _date;
    },
    momentDate(_date) {
      _date = _moment(_date)
        .format("YYYY-MM-DDTHH:mm:ss.SSS")
        .toString();
      _date = this.FixDateEntry(_date);
      return _date;
    },
    momentDateCompare(refDate, compareToDate) {
      return _moment(this.FixDateEntry(refDate)).isBefore(
        this.FixDateEntry(compareToDate)
      );
    },
    generateKey(len) {
      this.keystring = encodeURIComponent(
        _randomgen.generate({
          length: len
        })
      );
    }
  },
  created() {
    this.generateKey(255); //255 length of generated random characters
    __applog("created..vue");
  },
  mounted() {
    _indexsignalr.Initialize();
    _httphelper.Initialize();
    __applog("mounted..vue");
  }
});
