var scripts = ["Scripts/jquery.signalR-2.3.0.min.js", "signalr/hubs"];
export default {
  Initialize: function (BASEURI, callback) {
    $.cachedScript(BASEURI + scripts[0])
      .fail((jqxhr, settings, exception) => {
        __applog("Exception: " + exception);
        __vueinstance__.$_httphelper.ExitWin();
      })
      .done((script, textStatus) => {
        if (textStatus === "success") {
          $.cachedScript(BASEURI + scripts[1]).done((script, textStatus) => {
            if (textStatus === "success") {
              this.ActivateSignalR((status, connID) => {
                callback(status, connID);
              });
            } else {
              alert("Please restart the application!");
            }
          }, true);
        } else {
          alert("Please restart the application!");
        }
      }, true);
  },
  EnableBackgroundMode: function (enable, callback) {
    //cordova.plugins.backgroundMode.overrideBackButton();
    // cordova.plugins.backgroundMode.excludeFromTaskList();
    if (enable) cordova.plugins.backgroundMode.enable();
    else cordova.plugins.backgroundMode.disable();
    //local notifications
    cordova.plugins.notification.local.setDefaults({
      led: { color: "#FF00FF", on: 500, off: 500 },
      vibrate: true,
      autoClear: true,
      silent: false
    });

    callback("OK");
  },
  SetPushNotification: function (title, text, issilent) {
    cordova.plugins.backgroundMode.setDefaults({
      title: title,
      text: text,
      icon: "icon", // this will look for icon.png in platforms/android/res/drawable|mipmap
      color: "EEEEEE", // hex format like 'F14F4D'
      resume: false,
      hidden: false,
      bigText: true,
      silent: issilent
    });
  },
  ShowLocalNotification: function (title, message, id) {
    cordova.plugins.notification.local.hasPermission(granted => {
      __applog("Notification Grant: " + granted + " / id: " + id);
      if (granted) {
        cordova.plugins.notification.local.schedule({
          title: title,
          text: message,
          foreground: true,
          data: {
            msgid: encodeURIComponent(id)
          },
          smallIcon: "res://drawable/icon.png",
          icon: "res://drawable/icon.png"
        });

        cordova.plugins.notification.local.on(
          "click",
          notification => {
            var obj = notification.data;
            __applog("Notification Click: " + obj.msgid);
            __vueinstance__.$router.push("/mymessage/" + obj.msgid); //fetch new data from server
            cordova.plugins.notification.local.cancel(1, () => { }, this);
          },
          this
        );
      }
    });
  },
  ActivateSignalR: function (callback) {
    try {
      jQuery.support.cors = true;
      $.connection.hub.logging = true;
      $.connection.hub.url =
        "https://adgtappsdev-analog.msappproxy.net/EISMobile/VirtualWall/signalr";

      this.NotifierToggle();

      $.connection.hub.disconnected(() => {
        setTimeout(() => {
          this.NotifierToggle();
        }, 5000);
      });
      $.connection.hub.connectionSlow(() => {
        __applog(
          "We are currently experiencing difficulties with the connection."
        );
        var dialog = __vueinstance__.$_bootbox.dialog({
          message:
            '<p class="text-center">We are currently experiencing difficulties with the connection.</p>',
          closeButton: false
        });
        // do something in the background
        dialog.modal("hide");
      });

      $.connection.hub.error(error => {
        __applog("SignalR error: " + error);
        $.connection.hub.stop();
      });

      $.connection.hub.reconnected(() => {
        callback("CONNECTED", $.connection.hub.id);
      });

      $.connection.hub.stateChanged(change => {
        if (change.newState === $.signalR.connectionState.connected) {
          callback("CONNECTED", $.connection.hub.id);
        } else {
          callback("NO_CONNECTED", JSON.stringify(change));
        }
      });
    } catch (e) {
      __applog("Exception/SignalR: " + e);
    }
  },
  NotifierToggle: function () {
    try {
      $.connection.userHub.client.replyWithDateLastUpdated = result => {
        // result
        // 0 - connectionid
        // 1 - sender (ntaccount)
        // 2 - datelastupdated (user)
        // 3 - blockowner (ntaccount),
        // 4 - Title,
        // 5 - Content
        this.ShowLocalNotification(
          decodeURIComponent(result[4]),
          decodeURIComponent(result[5]),
          result[2]
        );
      };
      var currentUser = localStorage.getItem(_user_id_storage());
      $.connection.hub.qs = {
        username: currentUser
      };
      $.connection.hub
        .start({
          pingInterval: 60000,
          waitForPageLoad: true,
          transport: ["longPolling", "webSockets"]
        })
        .done(() => {
          __applog("Connected, transport = " + $.connection.hub.transport.name);
          // try {
          //   var con = $.connection.hub.id;
          //   __applog("Con ID: " + con);
          //   if (con != null && con !== "") {
          //     if (currentUser != null) {
          //       try {
          //         setInterval(()=> {
          //           __applog("Pinging SignalR Host...");
          //           $.connection.userHub.server
          //             .initiatePing(currentUser, device.serial)
          //             .done((result)=> {
          //               __applog("Ping Response: " + JSON.stringify(result));
          //             });
          //         }, 1000 * 180);
          //       } catch (ex) {
          //         __applog("Ping Server: " + JSON.stringify(ex));
          //       }
          //     }
          //   }
          // } catch (e) {
          //   __applog(e);
          // }
        });
    } catch (e) {
      __applog("Notifier: " + e);
    }
  }
};

///Initialize the jquery instance
jQuery.cachedScript = (url, options, cache) => {
  __applog("URL: " + url);
  options = $.extend(options || {}, {
    dataType: "script",
    cache: cache,
    url: url
  });
  return jQuery.ajax(options);
};
