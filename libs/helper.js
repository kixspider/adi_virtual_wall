var __authContext__ = null;
var __vueinstance__ = null;

/* Global constants */
const RESOURCE = "https://graph.windows.net/";
const AUTHORITY_URL = "https://login.windows.net/analog.onmicrosoft.com";
const CLIENT_ID = "0842ceec-49f9-4780-95c0-8dc80b7591bc";
const REDIRECT_URI = "https://adgtappsdev-analog.msappproxy.net";
const BASEURI = REDIRECT_URI + "/EISMobile/VirtualWall/";
const LOGOUT_URI = AUTHORITY_URL + "/oauth2/logout?post_logout_redirect_uri=";

/**
 * 
 * @param {Application logger / Log data to be printed on console} log 
 */
const __applog = function (log) {
    console.log(log);
}

/**
 * Conversation Tag
 */
const _convo_tag = function () {
    return "__convo_tag__";
}

/**
 * Connection ID (Signal R)
 */
const _con_id = function () {
    return "__con_id__";
}

/**
 * Phone ID
 */
const _phone_tag = function () {
    return "__phone_tag__";
}

/**
 * User Email
 */
const _user_email = function () {
    return "__user_email__";
}

/**
 * Inbox Holder
 */
const _inbox_json = function () {
    return "__inbox__";
}

/**
 * User Holder
 */
const _user_info_json = function () {
    return "__user_info__";
}

/**
 * Image File Holder
 */
const _image_file_json = function () {
    return "__image__";
}

/**
 * Block Owner Holder
 */
const _block_owner_json = function () {
    return "__block_owner__";
}

/**
 * Device ID Holder
 */
const _device_id_storage = function () {
    return "__device_id__";
}

/**
 * User ID Holder
 */
const _user_id_storage = function () {
    return "__user_id__";
}

/**
 * 
 * @param {Query name to be checked} name 
 * @param {URL of application} url 
 */
const query_string = function (name, url) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}