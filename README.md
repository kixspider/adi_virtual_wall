# Virtual Wall - Mobile

**Virtual Wall - Mobile** is a cross-platform mobile application that allows the users to submit feedback on ADGT's Enterprise Aligment Wall by scanning the QR codes found on the each block. It also allows the block owners to receive the feedback sent by the users and reply to them.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installation

Before going through the rest of the installation guidelines, make sure you have Node.js and its package manager installed on your computer. Check out this [link](https://www.npmjs.com/get-npm?utm_source=house&utm_medium=homepage&utm_campaign=free%20orgs&utm_term=Install%20npm) for more information.

Open your console and check if you have Cordova installed:

```shell
cordova --v
```

If the command is not recognized by the system, install Cordova through NPM (Node.js Package Manager):

```shell
npm install -g cordova
```

Once you've installed Cordova, go to the directory where you want to store this project:

```shell
cd path/to/directory
```

Clone this repository:

```shell
git clone https://ERodulfo@bitbucket.analog.com/scm/adgtsegeis/virtualwall-vue.git
```

Go to cloned repositoty by entering:

```shell
cd virtualwall-vue
```

Generate and fetch the plugins used in this project from the config file:

```shell
cordova prepare
```

Don't forget to set up each platform's development environment! Check out Cordova's official [platform guides](https://cordova.apache.org/docs/en/4.0.0/guide/platforms/) for more information.

In case you encounter an error regarding the installation of platforms and/or plugins, try to manually add and remove them as follows:

```shell
cordova platform remove <platform-name>
cordova platform add <platform-name>

cordova plugin remove <plugin-name>
cordova plugin add <plugin-name>
```

## Build Setup

Instruction for codova building and code compilation.

``` bash
# install dependencies
npm install

# compiled src and push to phone in debug mode
npm run android-dev

# build for production with minification
npm run android-release

```

To know more of the framework, please go to this link: https://vuejs.org

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).




```bash
Powered by: Engineering Systems